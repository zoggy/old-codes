Old unmaintained OCaml codes. Git history of each code stands
in a separate branch. Tags were copied too.

- [Caml-get](../camlget-master) was a tool to distribute and get Objective-Caml code, in a way similar to the apt-get utility https://zoggy.good-eris.net/camlget.html.en
- [Config-file](../config-file-master) was library to define, read and write configuration options and files
- [DBForge](../dbforge-master) was a tool to describe database schemas and generate OCaml code to access these databases.
- [Genet](../genet-master) was a tool to build continuous integration platforms.
- [Gtktop](../gtktop-master) was a library to create graphical toplevel in ocaml.
- [Mp3tag](../mp3tag-master) was a library to read/write tags in mp3 files.
- [OCaml-CEF](../ocaml-cef-master) were bindings to Chromium Embedded Framework library.
- [OCaml-lxc](../ocaml-lxc-master) were bindings to the lxc library.
- [OCaml-openmaple](../ocaml-openmaple-master) were bindings to opemmaple library.
- [OCaml-socket-daemon](../ocaml-socket-daemon-master) was a library to control a server through a socket.
- [Taglog](../ocaml-taglog-master) was a logging library with the ability to tag and filter messages.
- [Testrunner](../ocaml-testrunner-master) was a tool to define and run tests.
- [OCamltop](../ocamltop-gtk-master) was a graphical OCaml toplevel.
- [Odiff-gtk](../odiff-gtk-master) was a library to display textual diffs in Gtk applications.
- [Oug](../oug-master) was an OCaml code analysis tool based on graphs.
- [Stog-rdf](../stog-rdf-master) was a plugin for Stog. It is now included in [Stog](https://www.good-eris.net/stog/plugins/rdf.html).
- [Stog-writing](../stog-writing-master) was a plugin for Stog. It is now included in [Stog](https://www.good-eris.net/stog/plugins/writing.html).
- [Warner](../warner-master) was a tool to display when a file was modified, used to check for incoming emails.

